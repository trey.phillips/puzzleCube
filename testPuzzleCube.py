from PuzzleCube import *
import pytest
from unittest.mock import patch

solutions = open("CubeFiles/testScrambleSolutions.txt", "r")


def test_solve1():
    test1 = PuzzleCube("testScramble1.txt")
    solution1 = solutions.readline().strip()
    test1.perform(solution1)
    assert test1.is_solved()


def test_solve2():
    test2 = PuzzleCube("testScramble2.txt")
    solution2 = solutions.readline().strip()
    test2.perform(solution2)
    assert test2.is_solved()


def test_game_loop():
    test = PuzzleCube()
    test.game_loop()