import random


class PuzzleCube:

    def __init__(self, file_name=None):
        """
        Initializes a PuzzleCube object derived from a .txt file named file_name
        @param file_name: String representing the name of a desired .txt file from which this cube will be created
        """
        cube_directory = "CubeFiles/"
        puzzle_file = open(cube_directory + file_name, "r") if file_name else open(cube_directory + "solvedCube.txt",
                                                                                   "r")

        self.name = file_name[:-4] if file_name else "NoName"
        puzzle_lines = puzzle_file.readlines()
        self.__array = [char for char in "".join([line.strip() for line in puzzle_lines])]
        self.action_history = []

        self.inds = [
            0, 1, 2,
            3, 4, 5,
            6, 7, 8,
            9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20,
            21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32,
            33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44,
            45, 46, 47,
            48, 49, 50,
            51, 52, 53
        ]

        self.up_inds = [
            0, 1, 2,
            3, 4, 5,
            6, 7, 8
        ]
        self.left_inds = [
            9, 10, 11,
            21, 22, 23,
            33, 34, 35
        ]
        self.front_inds = [
            12, 13, 14,
            24, 25, 26,
            36, 37, 38
        ]
        self.right_inds = [
            15, 16, 17,
            27, 28, 29,
            39, 40, 41
        ]
        self.back_inds = [
            18, 19, 20,
            30, 31, 32,
            42, 43, 44
        ]
        self.down_inds = [
            45, 46, 47,
            48, 49, 50,
            51, 52, 53
        ]
        if not file_name:
            random_moves = random.sample(["f", "F", "r", "R", "b", "B", "l", "L", "u", "U", "d", "D"] * 40, 40)
            self.perform(random_moves)
            self.action_history = []

    def __repr__(self):
        """
        Defines the string representation for this PuzzleCube
        @return: Multiline String representing the current state of this Puzzle Cube
        """
        inds = [
            0, 47,
            3, 1, 46, 50,
            6, 4, 2, 45, 49, 53,
            7, 5, 48, 52,
            8, 51,
            12, 17, 35, 42,
            13, 16, 34, 43,
            24, 14, 15, 29, 23, 33, 44, 30,
            25, 28, 22, 31,
            36, 26, 27, 41, 11, 21, 32, 18,
            37, 40, 10, 19,
            38, 39, 9, 20
        ]

        self.color_str = ''.join([self.__array[i] for i in inds])

        return """
            x                  x
          / {} \\              / {} \\
        / {}   {}  \\         / {}   {}  \\
     /  {}   {}   {}  \\    /  {}   {}   {}  \\
    |\\    {}   {}    /|  |\\    {}   {}    /|
    |   \\   {}   /   |  |   \\   {}   /   |
    | {}    \\ /    {} |  | {}    \\ /    {} |
    |   {}   |   {}   |  |   {}   |   {}   |
    | {}   {} | {}   {} |  | {}   {} | {}   {} |
    |   {}   |   {}   |  |   {}   |   {}   |
    | {}   {} | {}   {} |  | {}   {} | {}   {} |
    \\   {}   |   {}   /  \\   {}   |   {}   /
       \\  {} | {}  /        \\  {} | {}  /
         \\  |  /            \\  |  /
            V                  V""".format(self.color_str[0], self.color_str[1], self.color_str[2], self.color_str[3],
                                           self.color_str[4], self.color_str[5], self.color_str[6], self.color_str[7],
                                           self.color_str[8], self.color_str[9], self.color_str[10], self.color_str[11],
                                           self.color_str[12], self.color_str[13], self.color_str[14],
                                           self.color_str[15], self.color_str[16], self.color_str[17],
                                           self.color_str[18], self.color_str[19], self.color_str[20],
                                           self.color_str[21], self.color_str[22], self.color_str[23],
                                           self.color_str[24], self.color_str[25], self.color_str[26],
                                           self.color_str[27], self.color_str[28], self.color_str[29],
                                           self.color_str[30], self.color_str[31], self.color_str[32],
                                           self.color_str[33], self.color_str[34], self.color_str[35],
                                           self.color_str[36], self.color_str[37], self.color_str[38],
                                           self.color_str[39], self.color_str[40], self.color_str[41],
                                           self.color_str[42], self.color_str[43], self.color_str[44],
                                           self.color_str[45], self.color_str[46], self.color_str[47],
                                           self.color_str[48], self.color_str[49], self.color_str[50],
                                           self.color_str[51], self.color_str[52], self.color_str[53])

    @property
    def array(self):
        return self.__array

    def is_solved(self):

        sides = [
            [self.__array[i] for i in self.front_inds],
            [self.__array[i] for i in self.right_inds],
            [self.__array[i] for i in self.back_inds],
            [self.__array[i] for i in self.left_inds],
            [self.__array[i] for i in self.up_inds],
            [self.__array[i] for i in self.down_inds]
        ]

        for side in sides:
            if len(set(side)) > 1:
                return False
        return True


    def f(self):
        """
        Turns the front face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [8, 7, 6, 11, 23, 35, 45, 46, 47, 39, 27, 15]
        edge_dest = [39, 27, 15, 8, 7, 6, 11, 23, 35, 45, 46, 47]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            36, 24, 12,
            37, 25, 13,
            38, 26, 14
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.front_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "F"

    def fp(self):
        """
        Turns the top face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [39, 27, 15, 8, 7, 6, 11, 23, 35, 45, 46, 47]
        edge_dest = [8, 7, 6, 11, 23, 35, 45, 46, 47, 39, 27, 15]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            14, 26, 38,
            13, 25, 37,
            12, 24, 36
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.front_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "F\'"

    def r(self):
        """
        Turns the right face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [2, 5, 8, 14, 26, 38, 47, 50, 53, 42, 30, 18]
        edge_dest = [42, 30, 18, 2, 5, 8, 14, 26, 38, 47, 50, 53]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            39, 27, 15,
            40, 28, 16,
            41, 29, 17
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.right_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "R"

    def rp(self):
        """
        Turns the right face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [42, 30, 18, 2, 5, 8, 14, 26, 38, 47, 50, 53]
        edge_dest = [2, 5, 8, 14, 26, 38, 47, 50, 53, 42, 30, 18]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            17, 29, 41,
            16, 28, 40,
            15, 27, 39
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.right_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "R\'"

    def b(self):
        """
        Turns the back face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [0, 1, 2, 17, 29, 41, 53, 52, 51, 33, 21, 9]
        edge_dest = [33, 21, 9, 0, 1, 2, 17, 29, 41, 53, 52, 51]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            42, 30, 18,
            43, 31, 19,
            44, 32, 20
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.back_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "B"

    def bp(self):
        """
        Turns the back face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [33, 21, 9, 0, 1, 2, 17, 29, 41, 53, 52, 51]
        edge_dest = [0, 1, 2, 17, 29, 41, 53, 52, 51, 33, 21, 9]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            20, 32, 44,
            19, 31, 43,
            18, 30, 42
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.back_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "B\'"

    def l(self):
        """
        Turns the left face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [6, 3, 0, 20, 32, 44, 51, 48, 45, 36, 24, 12]
        edge_dest = [36, 24, 12, 6, 3, 0, 20, 32, 44, 51, 48, 45]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            33, 21, 9,
            34, 22, 10,
            35, 23, 11
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.left_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "L"

    def lp(self):
        """
        Turns the left face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [36, 24, 12, 6, 3, 0, 20, 32, 44, 51, 48, 45]
        edge_dest = [6, 3, 0, 20, 32, 44, 51, 48, 45, 36, 24, 12]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            11, 23, 35,
            10, 22, 34,
            9, 21, 33
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.left_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "L\'"

    def u(self):
        """
        Turns the top face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [11, 10, 9, 20, 19, 18, 17, 16, 15, 14, 13, 12]
        edge_dest = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            6, 3, 0,
            7, 4, 1,
            8, 5, 2
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.up_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr
        return "U"

    def up(self):
        """
        Turns the top face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9]
        edge_dest = [11, 10, 9, 20, 19, 18, 17, 16, 15, 14, 13, 12]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            2, 5, 8,
            1, 4, 7,
            0, 3, 6
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.up_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "U\'"

    def d(self):
        """
        Turns the bottom face of this PuzzleCube 90 degrees clockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [38, 37, 36, 35, 34, 33, 44, 43, 42, 41, 40, 39]
        edge_dest = [41, 40, 39, 38, 37, 36, 35, 34, 33, 44, 43, 42]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            51, 48, 45,
            52, 49, 46,
            53, 50, 47
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.down_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "D"

    def dp(self):
        """
        Turns the bottom face of this PuzzleCube 90 degrees counterclockwise
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        working_arr = self.__array

        edge = [41, 40, 39, 38, 37, 36, 35, 34, 33, 44, 43, 42]
        edge_dest = [38, 37, 36, 35, 34, 33, 44, 43, 42, 41, 40, 39]
        edge_str = "".join([working_arr[ind] for ind in edge])
        for i in range(12):
            working_arr[edge_dest[i]] = edge_str[i]

        face_dest = [
            47, 50, 53,
            46, 49, 52,
            45, 48, 51
        ]
        temp_arr = [working_arr[i] for i in face_dest]

        face = self.down_inds

        for i in range(9):
            working_arr[face[i]] = temp_arr[i]

        self.__array = working_arr

        return "D\'"

    def rotate_90_vert(self, direction):
        """
        Rotates this PuzzleCube along the x-axis or z-axis in a given direction
        @param direction: Char representing (f)orward, (r)ight, (b)ack, or (l)eft
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        if direction == "f":

            new_inds = [
                            44, 43, 42,
                            32, 31, 30,
                            20, 19, 18,
                33, 21,  9,  0,  1,  2, 17, 29, 41, 53, 52, 51,
                34, 22, 10,  3,  4,  5, 16, 28, 40, 50, 49, 48,
                35, 23, 11,  6,  7,  8, 15, 27, 39, 47, 46, 45,
                            12, 13, 14,
                            24, 25, 26,
                            36, 37, 38
            ]

        elif direction == "r":

            new_inds = [
                            33, 21,  9,
                            34, 22, 10,
                            35, 23, 11,
                51, 48, 45, 36, 24, 12,  6,  3,  0, 20, 32, 44,
                52, 49, 46, 37, 25, 13,  7,  4,  1, 19, 31, 43,
                53, 50, 47, 38, 26, 14,  8,  5,  2, 18, 30, 42,
                            39, 27, 15,
                            40, 28, 16,
                            41, 29, 17
            ]

        elif direction == "b":

            new_inds = [
                            12, 13, 14,
                            24, 25, 26,
                            36, 37, 38,
                11, 23, 35, 45, 46, 47, 39, 27, 15,  8,  7,  6,
                10, 22, 34, 48, 49, 50, 40, 28, 16,  5,  4,  3,
                 9, 21, 33, 51, 52, 53, 41, 29, 17,  2,  1,  0,
                            44, 43, 42,
                            32, 31, 30,
                            20, 19, 18
            ]

        elif direction == "l":

            new_inds = [
                            17, 29, 41,
                            16, 28, 40,
                            15, 27, 39,
                 2,  5,  8, 14, 26, 38, 47, 50, 53, 42, 30, 18,
                 1,  4,  7, 13, 25, 37, 46, 49, 52, 43, 31, 19,
                 0,  3,  6, 12, 24, 36, 45, 48, 51, 44, 32, 20,
                            11, 23, 35,
                            10, 22, 34,
                             9, 21, 33
            ]

        else:
            new_inds = self.inds

        temp_arr = [self.__array[i] for i in new_inds]
        self.__array = temp_arr

        rotation_90_vert_dict = {"b": "x", "f": "x\'", "r": "z", "l": "z\'"}
        return rotation_90_vert_dict[direction]

    def rotate_90_hor(self, direction):
        """
        Rotates this PuzzleCube along the y-axis in a given direction
        @param direction: Char representing (l)eft or (r)ight
        @return: String representing the standard notation for performing this action on a PuzzleCube
        """
        if direction == "r":

            new_inds = [
                             2,  5,  8,
                             1,  4,  7,
                             0,  3,  6,
                18, 19, 20,  9, 10, 11, 12, 13, 14, 15, 16, 17,
                30, 31, 32, 21, 22, 23, 24, 25, 26, 27, 28, 29,
                42, 43, 44, 33, 34, 35, 36, 37, 38, 39, 40, 41,
                            51, 48, 45,
                            52, 49, 46,
                            53, 50, 47
            ]
        elif direction == "l":

            new_inds = [
                             6,  3,  0,
                             7,  4,  1,
                             8,  5,  2,
                12, 13, 14, 15, 16, 17, 18, 19, 20,  9, 10, 11,
                24, 25, 26, 27, 28, 29, 30, 31, 32, 21, 22, 23,
                36, 37, 38, 39, 40, 41, 42, 43, 44, 33, 34, 35,
                            47, 50, 53,
                            46, 49, 52,
                            45, 48, 51
            ]

        else:
            new_inds = self.inds

        temp_arr = [self.__array[i] for i in new_inds]
        self.__array = temp_arr

        return "y" if direction == "l" else "y\'"

    # noinspection PyMethodMayBeStatic
    def perform(self, s):
        """
        Performs actions upon this PuzzleCube based on a given string
        @param s: Iterable consisting of any arrangement of the following characters: frbludxyzFRBLUDXYZ
        @return: Array representing all actions that have been performed on this PuzzleCube due to calling this function
        """
        rotation_dict = {
            "y": "rotate_90_hor(\"l\")",
            "Y": "rotate_90_hor(\"r\")",
            "x": "rotate_90_vert(\"b\")",
            "X": "rotate_90_vert(\"f\")",
            "z": "rotate_90_vert(\"r\")",
            "Z": "rotate_90_vert(\"l\")",
        }
        action_count = len(self.action_history)
        for command in s:

            if command not in "fFrRbBlLuUdDxXyYzZ":
                raise Exception("Invalid Move Detected")
            elif command in rotation_dict.keys():
                op = f"self.{rotation_dict[command]}"
            else:
                end = "p()" if command.isupper() else "()"
                op = f"self.{command.lower()}{end}"

            self.action_history.append(eval(op))

        return self.action_history[action_count:]

    def game_loop(self):
        """
        Operates a game loop that allows the user to manipulate this cube for a desired amount of time
        @return: Array representing all actions performed on this PuzzleCube during this function call
        """
        print(self)
        print("Enter \"q\" at any point to escape")
        key_in = ""
        action_count = len(self.action_history)
        while key_in != "q":

            key_in = input(
                f"Which action(s) would you like to perform on {self.name}? "
            )

            if key_in.lower() == "q":
                break
            elif key_in == "moves":
                print("Possible commands: f, F, l, L, r, R, u, U, d, D, x, X, y, Y, z, Z")
            else:
                self.perform(key_in)
                print(self)

        return self.action_history[action_count:]
